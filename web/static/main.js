const [
  email,
  password,
  emailLabel,
  pwdLabel,
  arrowDown,
  languageNav,
  currentLanguage,
  usernameError,
  passwordError,
  loginForm,
  loginError,
  encrypt,
] = [
  document.querySelector("#login"),
  document.querySelector("#password"),
  document.querySelector(".theme-form-label"),
  document.querySelector("#password-label"),
  document.querySelector(".arrow-base.down"),
  document.querySelector(".nav"),
  document.querySelector(".current-language"),
  document.querySelector("#login-error-username"),
  document.querySelector("#login-error-password"),
  document.querySelector("#login-form"),
  document.querySelector("#login-error"),
  document.querySelector("#encrypt"),
];

const LOCALE_KEY = 'alauda_locale';
var loginHomeUri = '/';
const authUri = 'api/v1/token/login'

document.addEventListener("DOMContentLoaded", () => {
  initLanguage();
  // get auth uri
  const uri = sessionStorage.getItem('_init_url')
  if (uri) {
    setRequest("GET", `${uri}${authUri}`, null, getAuthUri)
  }

  // init labels' status
  if (email && email.value) {
    active(emailLabel);
  }
  if (password && password.value) {
    active(pwdLabel);
  }

  if (email) {
    email.addEventListener("focus", () => {
      active(emailLabel);
    });
    email.addEventListener("blur", () => blur(email, emailLabel));
  }

  if (password) {
    password.addEventListener("focus", () => {
      active(pwdLabel);
    });
    password.addEventListener("blur", () => blur(password, pwdLabel));
  }
});

function active(label) {
  !label.classList.contains("theme-form-label--active") &&
    label.classList.add("theme-form-label--active");
}

function blur(el, label) {
  !el.value && label.classList.remove("theme-form-label--active");
}

function setLanguage(language) {
  const _currentLanguage = localStorage.getItem(LOCALE_KEY);
  localStorage.setItem(LOCALE_KEY, language);
  setCookie(LOCALE_KEY, language);
  languageNav.style.display = "none";
  currentLanguage.innerText = language === 'zh' ? '简体中文' : 'English';
  if (language != _currentLanguage) {
    location.reload();
  }
}

function setCookie(key, value) {
  document.cookie = `${key}=${value};path=/`;
}

function initLanguage() {
  let cookieLanguage = getCookie(LOCALE_KEY);
  let language = localStorage.getItem(LOCALE_KEY);
  if (!language) {
    language = 'zh';
  }
  if (cookieLanguage != language) {
    setCookie(LOCALE_KEY, language);
    location.reload();
  }
  setLanguage(language);
}

function showLanguageNav() {
  languageNav.style.display = languageNav.style.display == "block" ? "none": "block";
}

function inputEnter(e) {
  if (loginError) {
    loginError.style.display = "none";
  }
  if (e.keyCode === 13) {
    e.preventDefault();
    submitForm()
  } else {
    usernameError.style.display = "none";
    passwordError.style.display = "none";
  }
}

function submitForm() {
  resetError();
  if (!email.value) {
    usernameError.style.display = "block";
    email.focus();
    return;
  }

  if (!password.value) {
    passwordError.style.display = "block";
    password.focus();
    return;
  }
  encrypt.value = btoa(password.value);
  loginForm.submit();
}

function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

function goBack() {
  window.location = loginHomeUri;
}

function setRequest(method, uri, data, callback) {
  const xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function () {
    if(xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
      callback(xhr.responseText)
    }
  }
  xhr.open(method, uri, true);
  xhr.send(data);
}

function getAuthUri(res) {
  try {
    loginHomeUri = JSON.parse(res).auth_url;
  } catch (e) {}
}

function resetError() {
  if (loginError) {
    loginError.style.display = "none";
  }
  usernameError.style.display = "none";
  passwordError.style.display = "none";
}