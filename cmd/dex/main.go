package main

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string
var labelBaseDomain string

var RootCmd = &cobra.Command{
	Use: "dex",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
		os.Exit(2)
	},
}

func init() {
	RootCmd.AddCommand(commandServe())
	RootCmd.AddCommand(commandVersion())

	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports Persistent Flags, which, if defined here,
	// will be global for your application.

	RootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/dex.yaml)")
	RootCmd.PersistentFlags().StringVar(&labelBaseDomain, "label-base-domain", "alauda.io", "The based domain of the resource label.")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" { // enable ability to specify config file via flag
		viper.SetConfigFile(cfgFile)
	}

	viper.SetConfigName("dex")   // name of config file (without extension)
	viper.AddConfigPath("$HOME") // adding home directory as first search path

	viper.SetDefault("PORTAL_NAMESPACE", "alauda-system")
	viper.SetDefault("PORTAL_CONFIG_NAME", "portal-configmap")
	viper.SetDefault("REFRESH_TOKEN_GC", 72)
	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

func main() {
	if err := RootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err.Error())
		os.Exit(2)
	}
}
