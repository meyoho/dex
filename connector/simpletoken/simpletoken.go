package simpletoken

import (
	"encoding/json"
	"github.com/sirupsen/logrus"
	"github.com/coreos/dex/connector"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
	"strconv"
	"net/url"
)

const (
	apiURL   = ""
)

type Config struct {
	BaseURL      string `json:"baseURL"`
	RedirectURI  string `json:"redirectURI"`
}

func (c *Config) Open(id string, logger logrus.FieldLogger) (connector.Connector, error) {
	if c.BaseURL == "" {
		c.BaseURL = apiURL
	}
	return &simpelTokenConnector{
		baseURL:      c.BaseURL,
		redirectURI:  c.RedirectURI,
		logger:       logger,
	}, nil
}

type simpelTokenConnector struct {
	baseURL      string
	redirectURI  string
	org          string
	clientID     string
	clientSecret string
	logger       logrus.FieldLogger
}

type simpleTokenResult struct {
	Code string		`json:"code"`
	Message string  `json:"message"`
	Data  simpleTokenResultData 	`json:"data"`
}

type simpleTokenResultData struct {
	Status int		`json:"status"`
	Message string  `json:"msg"`
	Data simpleTokenResultDataData `json:"data"`
}

type simpleTokenResultDataData struct {
	ID int	`json:"id"`
	Name string	`json:"name"`
	Isadmin int	`json:"isadmin"`
	Email string	`json:"email"`
	Phone string	`json:"phone"`
	Region string	`json:"region"`
}

func (ts *simpelTokenConnector) Login(token string) (connector.Identity, bool, error)  {
	// Get
	httpClient := &http.Client{
		Timeout: 10 * time.Second,
	}

	//res, err := httpClient.Get("http://192.168.2.155:9091/api/v1/test")
	req, err := http.NewRequest("GET", ts.baseURL, nil)
	//req.Header.Set("Content-Type", "application/json")
	//fmt.Printf("token1: %s \n token2:%s", token, "XIp%2BiOoLEks31As26%2FdsQOVsgALX1oNI%2FZjUxurW%2By3s2Q1a2rq3%2FnK9%2F%2BcQak%2F83ycjdlpZKuW3VDWwo53r%2BLJs0xXHPJvxc3Uz6I1HsAitEXIlGtDEWVRBGylgT2NJvWFmGnJQ5uJWx30rTPL6l7wSpNd5paBtUO%2F18Kslyc8%3D")

	req.Header.Set("X-Auth-Token", url.QueryEscape(token))
	res, err := httpClient.Do(req)
	if err != nil {
		fmt.Printf("Get SimpleToken server err: %s", err.Error())
		return connector.Identity{}, false, err
	}

	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return connector.Identity{}, false, err
	}

	fmt.Println(string(body))

	var result simpleTokenResult
	if err := json.Unmarshal(body, &result); err != nil {
		//panic(err)
		return connector.Identity{}, false, err
	}
	fmt.Printf("SimpleToken result: %+v \n", result)

	code, _ := strconv.Atoi(result.Code)
	if code != 0 || result.Data.Status != 0 {
		return connector.Identity{}, false, nil
	}

	return connector.Identity{
		UserID:        fmt.Sprintf("%d", result.Data.Data.ID),
		Username:      result.Data.Data.Name,
		Email:         result.Data.Data.Email,
		EmailVerified: true,
		Isadmin:       result.Data.Data.Isadmin,
	}, true, nil
}

func (ts *simpelTokenConnector)RedirectURI()string  {
	return ts.redirectURI
}