package kubernetes

import (
	"context"
	"errors"
	"fmt"
	"github.com/coreos/dex/server"
	"sort"
	"strings"
	"time"

	"github.com/spf13/viper"

	"github.com/coreos/dex/storage"
	"github.com/coreos/dex/storage/kubernetes/k8sapi"
	"github.com/sirupsen/logrus"
)

const (
	kindAuthCode        = "AuthCode"
	kindAuthRequest     = "AuthRequest"
	kindClient          = "OAuth2Client"
	kindRefreshToken    = "RefreshToken"
	kindKeys            = "SigningKey"
	kindPassword        = "Password"
	kindOfflineSessions = "OfflineSessions"
	kindConnector       = "Connector"
	KindConfigMap       = "ConfigMap"
	KindUser            = "User"
	KindGroup           = "Group"
	KindGroupBinding    = "GroupBinding"
)

const (
	resourceAuthCode        = "authcodes"
	resourceAuthRequest     = "authrequests"
	resourceClient          = "oauth2clients"
	resourceRefreshToken    = "refreshtokens"
	resourceKeys            = "signingkeies" // Kubernetes attempts to pluralize.
	resourcePassword        = "passwords"
	resourceOfflineSessions = "offlinesessionses" // Again attempts to pluralize.
	resourceConnector       = "connectors"
	resourceConfigMap       = "configmaps"
	resourceUser            = "users"
	resourceGroup           = "groups"
	resourceGroupBinding    = "groupbindings"
)

const (
	RBACApiGroup                = "rbac.authorization.k8s.io"
	RBACApiV1                   = "rbac.authorization.k8s.io/v1"
	resourceClusterrolebindings = "clusterrolebindings"
	resourceRolebindings        = "rolebindings"
	AuthGroupVersion            = "auth.alauda.io/v1"
	AuthGroup                   = "auth.alauda.io"
)

// Config values for the Kubernetes storage type.
type Config struct {
	InCluster      bool   `json:"inCluster"`
	KubeConfigFile string `json:"kubeConfigFile"`
	UseTPR         bool   `json:"useTPR"` // Flag option to use TPRs instead of CRDs
}

// Open returns a storage using Kubernetes third party resource.
func (c *Config) Open(logger logrus.FieldLogger) (storage.Storage, error) {
	cli, err := c.open(logger, false)
	if err != nil {
		return nil, err
	}
	return cli, nil
}

// open returns a kubernetes client, initializing the third party resources used
// by dex.
//
// waitForResources controls if errors creating the resources cause this method to return
// immediately (used during testing), or if the client will asynchronously retry.
func (c *Config) open(logger logrus.FieldLogger, waitForResources bool) (*client, error) {
	if c.InCluster && (c.KubeConfigFile != "") {
		return nil, errors.New("cannot specify both 'inCluster' and 'kubeConfigFile'")
	}
	if !c.InCluster && (c.KubeConfigFile == "") {
		return nil, errors.New("must specify either 'inCluster' or 'kubeConfigFile'")
	}

	var (
		cluster   k8sapi.Cluster
		user      k8sapi.AuthInfo
		namespace string
		err       error
	)
	if c.InCluster {
		cluster, user, namespace, err = inClusterConfig()
	} else {
		cluster, user, namespace, err = loadKubeConfig(c.KubeConfigFile)
	}
	if err != nil {
		return nil, err
	}

	cli, err := newClient(cluster, user, namespace, logger, c.UseTPR)
	if err != nil {
		return nil, fmt.Errorf("create client: %v", err)
	}

	ctx, cancel := context.WithCancel(context.Background())

	logger.Info("creating custom Kubernetes resources")
	if !cli.registerCustomResources(c.UseTPR) {
		if waitForResources {
			cancel()
			return nil, fmt.Errorf("failed creating custom resources")
		}

		// Try to synchronously create the custom resources once. This doesn't mean
		// they'll immediately be available, but ensures that the client will actually try
		// once.
		logger.Errorf("failed creating custom resources: %v", err)
		go func() {
			for {
				if cli.registerCustomResources(c.UseTPR) {
					return
				}

				select {
				case <-ctx.Done():
					return
				case <-time.After(30 * time.Second):
				}
			}
		}()
	}

	if waitForResources {
		if err := cli.waitForCRDs(ctx); err != nil {
			cancel()
			return nil, err
		}
	}

	// If the client is closed, stop trying to create resources.
	cli.cancel = cancel
	return cli, nil
}

// registerCustomResources attempts to create the custom resources dex
// requires or identifies that they're already enabled. This function creates
// third party resources(TPRs) or custom resource definitions(CRDs) depending
// on the `useTPR` flag passed in as an argument.
// It logs all errors, returning true if the resources were created successfully.
//
// Creating a custom resource does not mean that they'll be immediately available.
func (cli *client) registerCustomResources(useTPR bool) (ok bool) {
	ok = true
	length := len(customResourceDefinitions)
	if useTPR {
		length = len(thirdPartyResources)
	}

	for i := 0; i < length; i++ {
		var err error
		var resourceName string

		if useTPR {
			r := thirdPartyResources[i]
			err = cli.postResource("extensions/v1beta1", "", "thirdpartyresources", r)
			resourceName = r.ObjectMeta.Name
		} else {
			r := customResourceDefinitions[i]
			err = cli.postResource("apiextensions.k8s.io/v1beta1", "", "customresourcedefinitions", r)
			resourceName = r.ObjectMeta.Name
		}

		if err != nil {
			switch err {
			case storage.ErrAlreadyExists:
				cli.logger.Infof("custom resource already created %s", resourceName)
			case storage.ErrNotFound:
				cli.logger.Errorf("custom resources not found, please enable the respective API group")
				ok = false
			default:
				cli.logger.Errorf("creating custom resource %s: %v", resourceName, err)
				ok = false
			}
			continue
		}
		cli.logger.Errorf("create custom resource %s", resourceName)
	}
	return ok
}

// waitForCRDs waits for all CRDs to be in a ready state, and is used
// by the tests to synchronize before running conformance.
func (cli *client) waitForCRDs(ctx context.Context) error {
	ctx, cancel := context.WithTimeout(ctx, time.Second*30)
	defer cancel()

	for _, crd := range customResourceDefinitions {
		for {
			err := cli.isCRDReady(crd.Name)
			if err == nil {
				break
			}

			cli.logger.Errorf("checking CRD: %v", err)

			select {
			case <-ctx.Done():
				return errors.New("timed out waiting for CRDs to be available")
			case <-time.After(time.Millisecond * 100):
			}
		}
	}
	return nil
}

// isCRDReady determines if a CRD is ready by inspecting its conditions.
func (cli *client) isCRDReady(name string) error {
	var r k8sapi.CustomResourceDefinition
	err := cli.getResource("apiextensions.k8s.io/v1beta1", "", "customresourcedefinitions", name, &r)
	if err != nil {
		return fmt.Errorf("get crd %s: %v", name, err)
	}

	conds := make(map[string]string) // For debugging, keep the conditions around.
	for _, c := range r.Status.Conditions {
		if c.Type == k8sapi.Established && c.Status == k8sapi.ConditionTrue {
			return nil
		}
		conds[string(c.Type)] = string(c.Status)
	}
	return fmt.Errorf("crd %s not ready %#v", name, conds)
}

func (cli *client) Close() error {
	if cli.cancel != nil {
		cli.cancel()
	}
	return nil
}

func (cli *client) CreateAuthRequest(a storage.AuthRequest) error {
	return cli.post(resourceAuthRequest, cli.fromStorageAuthRequest(a))
}

func (cli *client) CreateClient(c storage.Client) error {
	return cli.post(resourceClient, cli.fromStorageClient(c))
}

func (cli *client) CreateAuthCode(c storage.AuthCode) error {
	return cli.post(resourceAuthCode, cli.fromStorageAuthCode(c))
}

func (cli *client) CreatePassword(p storage.Password) error {
	return cli.post(resourcePassword, cli.fromStoragePassword(p))
}

func (cli *client) CreateRefresh(r storage.RefreshToken) error {
	return cli.post(resourceRefreshToken, cli.fromStorageRefreshToken(r))
}

func (cli *client) CreateOfflineSessions(o storage.OfflineSessions) error {
	return cli.post(resourceOfflineSessions, cli.fromStorageOfflineSessions(o))
}

func (cli *client) CreateConnector(c storage.Connector) error {
	return cli.post(resourceConnector, cli.fromStorageConnector(c))
}

func (cli *client) CreateAuthConfigMap(authConfig storage.AuthConfig) error {
	err := cli.postResource("v1", cli.namespace, resourceConfigMap, cli.fromAuthConfigMap(authConfig))
	if err != nil {
		switch err {
		case storage.ErrAlreadyExists:
			cli.logger.Infof("auth configmap already created %s", authConfig.Name)
			err = cli.UpdateAuthConfigMap(authConfig.Name, authConfig,
				func(old, new storage.AuthConfig) (storage.AuthConfig, error) {
					switch {
					case new.AuthIssuer != "":
						old.AuthIssuer = new.AuthIssuer
						fallthrough
					case new.AuthIssuerInner != "":
						old.AuthIssuerInner = new.AuthIssuerInner
						fallthrough
					case new.ClientID != "":
						old.ClientID = new.ClientID
						fallthrough
					case new.ResponseType != "":
						old.ResponseType = new.ResponseType
						fallthrough
					default:
						old.ClientSecret = new.ClientSecret
						if new.RootCA != "" {
							old.RootCA = new.RootCA
						}
						old.RootCAFilePath = new.RootCAFilePath
						old.RedirectUri = new.RedirectUri
						old.Scopes = new.Scopes
						old.Enabled = new.Enabled
						old.AuthNamespace = new.AuthNamespace
						old.CustomRedirectUri = new.CustomRedirectUri
					}
					return old, nil
				})
			if err != nil {
				cli.logger.Errorf("update configmap %s : %s", authConfig.Name, err.Error())
				return err
			}
			return nil
		default:
			cli.logger.Errorf("creating auth configmap %s: %v", authConfig.Name, err)
			return err
		}
	}
	cli.logger.Errorf("create auth configmap %s", authConfig.Name)
	return nil
}

func (cli *client) CreateUser(user storage.User) error {
	if oldUser, err := cli.getUser(user.EmailToName()); err != nil {
		crd := cli.fromUser(user)
		//cli.logger.Infof("CreateUser.usercrd: %+v", crd)
		return cli.postResource(AuthGroupVersion, "", resourceUser, crd)
	} else {
		if !cli.isNeedUpdate(oldUser, user) {
			return nil
		}
		oldUser.UpdateFromUser(user)
		//cli.logger.Infof("Update.usercrd: %+v", oldUser)
		return cli.putResource(AuthGroupVersion, "", resourceUser, user.EmailToName(), oldUser)
	}
}

func (cli *client) isNeedUpdate(found User, user storage.User) bool {
	if found.Spec.IsAdmin &&
		found.Spec.ConnectorType == server.LocalConnector &&
		user.ConnectorType != server.LocalConnector {
		return false
	}
	connID, ok := found.Labels[storage.LabelUserConnectorID]
	if !ok {
		return true
	}
	if len(found.Annotations) == 0 {
		return true
	}
	if len(found.Spec.Groups) > 0 {
		sort.Strings(found.Spec.Groups)
	}
	if len(user.Groups) > 0 {
		sort.Strings(user.Groups)
	}
	if found.Spec.ConnectorName != user.ConnectorName ||
		found.Spec.ConnectorType != user.ConnectorType ||
		found.Spec.Valid != true ||
		found.Spec.UserName != user.UserName ||
		strings.Join(found.Spec.Groups, "") != strings.Join(user.Groups, "") ||
		found.Spec.Account != user.Account ||
		found.Spec.IsAdmin != user.IsAdmin ||
		connID != user.ConnectorID {
		return true
	}

	return false
}

func (cli *client) getUser(name string) (User, error) {
	var user User
	if err := cli.getResource(AuthGroupVersion, "", resourceUser, name, &user); err != nil {
		return User{}, err
	}
	return user, nil
}

func (cli *client) GetAuthRequest(id string) (storage.AuthRequest, error) {
	var req AuthRequest
	if err := cli.get(resourceAuthRequest, id, &req); err != nil {
		return storage.AuthRequest{}, err
	}
	return toStorageAuthRequest(req), nil
}

func (cli *client) GetAuthCode(id string) (storage.AuthCode, error) {
	var code AuthCode
	if err := cli.get(resourceAuthCode, id, &code); err != nil {
		return storage.AuthCode{}, err
	}
	return toStorageAuthCode(code), nil
}

func (cli *client) GetClient(id string) (storage.Client, error) {
	c, err := cli.getClient(id)
	if err != nil {
		return storage.Client{}, err
	}
	return toStorageClient(c), nil
}

func (cli *client) getClient(id string) (Client, error) {
	var c Client
	name := cli.idToName(id)
	if err := cli.get(resourceClient, name, &c); err != nil {
		return Client{}, err
	}
	if c.ID != id {
		return Client{}, fmt.Errorf("get client: ID %q mapped to client with ID %q", id, c.ID)
	}
	return c, nil
}

func (cli *client) GetPassword(emailOrAccount string) (storage.Password, error) {
	p, err := cli.getPassword(emailOrAccount)
	if err != nil {
		return storage.Password{}, err
	}
	return toStoragePassword(p), nil
}

func (cli *client) getPassword(emailOrAccount string) (Password, error) {
	// TODO(ericchiang): Figure out whose job it is to lowercase emails.
	// process email
	if strings.Contains(emailOrAccount, "@") {
		var p Password
		email := strings.ToLower(emailOrAccount)
		name := storage.GetMD5Hash(email)
		if err := cli.get(resourcePassword, name, &p); err != nil {
			cli.logger.Info("getPassword err:", err)
			return Password{}, err
		}
		if email != p.Email {
			return Password{}, fmt.Errorf("get email: email %q mapped to password with email %q", email, p.Email)
		}
		cli.logger.Info("getPassword's isAdmin: ", p.IsAdmin, ",email: ", p.Email)
		return p, nil
	} else {
		passwords, err := cli.ListPasswords()
		if err != nil {
			return Password{}, err
		}
		for _, password := range passwords {
			if password.Account == emailOrAccount {
				return Password{
					Email:    password.Email,
					Hash:     password.Hash,
					Username: password.Username,
					UserID:   password.UserID,
					Account:  password.Account,
					IsAdmin:  password.IsAdmin,
				}, nil
			}
		}
		return Password{}, fmt.Errorf("account '%s' not exists", emailOrAccount)
	}
}

func (cli *client) GetKeys() (storage.Keys, error) {
	var keys Keys
	if err := cli.get(resourceKeys, keysName, &keys); err != nil {
		return storage.Keys{}, err
	}
	return toStorageKeys(keys), nil
}

func (cli *client) GetRefresh(id string) (storage.RefreshToken, error) {
	r, err := cli.getRefreshToken(id)
	if err != nil {
		return storage.RefreshToken{}, err
	}
	return toStorageRefreshToken(r), nil
}

func (cli *client) getRefreshToken(id string) (r RefreshToken, err error) {
	err = cli.get(resourceRefreshToken, id, &r)
	return
}

func (cli *client) GetOfflineSessions(userID string, connID string) (storage.OfflineSessions, error) {
	o, err := cli.getOfflineSessions(userID, connID)
	if err != nil {
		return storage.OfflineSessions{}, err
	}
	return toStorageOfflineSessions(o), nil
}

func (cli *client) getOfflineSessions(userID string, connID string) (o OfflineSessions, err error) {
	name := cli.offlineTokenName(userID, connID)
	if err = cli.get(resourceOfflineSessions, name, &o); err != nil {
		return OfflineSessions{}, err
	}
	if userID != o.UserID || connID != o.ConnID {
		return OfflineSessions{}, fmt.Errorf("get offline session: wrong session retrieved")
	}
	return o, nil
}

func (cli *client) GetConnector(id string) (storage.Connector, error) {
	var c Connector
	if err := cli.get(resourceConnector, id, &c); err != nil {
		return storage.Connector{}, err
	}
	return toStorageConnector(c), nil
}

func (cli *client) ListClients() ([]storage.Client, error) {
	return nil, errors.New("not implemented")
}

func (cli *client) ListRefreshTokens() ([]storage.RefreshToken, error) {
	return nil, errors.New("not implemented")
}

func (cli *client) ListPasswords() (passwords []storage.Password, err error) {
	var passwordList PasswordList
	if err = cli.list(resourcePassword, &passwordList); err != nil {
		return passwords, fmt.Errorf("failed to list passwords: %v", err)
	}

	for _, password := range passwordList.Passwords {
		p := storage.Password{
			Email:    password.Email,
			Hash:     password.Hash,
			Username: password.Username,
			UserID:   password.UserID,
			Account:  password.Account,
			IsAdmin:  password.IsAdmin,
		}
		passwords = append(passwords, p)
	}

	return
}

func (cli *client) ListConnectors() (connectors []storage.Connector, err error) {
	var connectorList ConnectorList
	if err = cli.list(resourceConnector, &connectorList); err != nil {
		return connectors, fmt.Errorf("failed to list connectors: %v", err)
	}

	connectors = make([]storage.Connector, len(connectorList.Connectors))
	for i, connector := range connectorList.Connectors {
		connectors[i] = toStorageConnector(connector)
	}

	return
}

func (cli *client) GetAuthConfig(name string) (storage.AuthConfig, error) {
	var authConfig AuthConfigMap

	if err := cli.getResource("v1", cli.namespace, resourceConfigMap, name, &authConfig); err != nil {
		return storage.AuthConfig{}, err
	}
	return toStorageAuthConfig(authConfig), nil
}

func (cli *client) DeleteAuthRequest(id string) error {
	return cli.delete(resourceAuthRequest, id)
}

func (cli *client) DeleteAuthCode(code string) error {
	return cli.delete(resourceAuthCode, code)
}

func (cli *client) DeleteClient(id string) error {
	// Check for hash collition.
	c, err := cli.getClient(id)
	if err != nil {
		return err
	}
	return cli.delete(resourceClient, c.ObjectMeta.Name)
}

func (cli *client) DeleteRefresh(id string) error {
	return cli.delete(resourceRefreshToken, id)
}

func (cli *client) DeletePassword(email string) error {
	// Check for hash collition.
	p, err := cli.getPassword(email)
	if err != nil {
		return err
	}
	if p.IsAdmin {
		return errors.New("admin users can't be delete")
	}
	return cli.delete(resourcePassword, p.ObjectMeta.Name)
}

func (cli *client) DeleteOfflineSessions(userID string, connID string) error {
	// Check for hash collition.
	o, err := cli.getOfflineSessions(userID, connID)
	if err != nil {
		return err
	}
	return cli.delete(resourceOfflineSessions, o.ObjectMeta.Name)
}

func (cli *client) DeleteConnector(id string) error {
	return cli.delete(resourceConnector, id)
}

func (cli *client) UpdateRefreshToken(id string, updater func(old storage.RefreshToken) (storage.RefreshToken, error)) error {
	r, err := cli.getRefreshToken(id)
	if err != nil {
		return err
	}
	updated, err := updater(toStorageRefreshToken(r))
	if err != nil {
		return err
	}
	updated.ID = id

	newToken := cli.fromStorageRefreshToken(updated)
	newToken.ObjectMeta = r.ObjectMeta
	return cli.put(resourceRefreshToken, r.ObjectMeta.Name, newToken)
}

func (cli *client) UpdateClient(id string, updater func(old storage.Client) (storage.Client, error)) error {
	c, err := cli.getClient(id)
	if err != nil {
		return err
	}

	updated, err := updater(toStorageClient(c))
	if err != nil {
		return err
	}
	updated.ID = c.ID

	newClient := cli.fromStorageClient(updated)
	newClient.ObjectMeta = c.ObjectMeta
	return cli.put(resourceClient, c.ObjectMeta.Name, newClient)
}

func (cli *client) UpdatePassword(email string, updater func(old storage.Password) (storage.Password, error)) error {
	p, err := cli.getPassword(email)
	if err != nil {
		return err
	}

	updated, err := updater(toStoragePassword(p))
	if err != nil {
		return err
	}
	updated.Email = p.Email

	newPassword := cli.fromStoragePassword(updated)
	newPassword.ObjectMeta = p.ObjectMeta
	return cli.put(resourcePassword, p.ObjectMeta.Name, newPassword)
}

func (cli *client) UpdateOfflineSessions(userID string, connID string, updater func(old storage.OfflineSessions) (storage.OfflineSessions, error)) error {
	o, err := cli.getOfflineSessions(userID, connID)
	if err != nil {
		return err
	}

	updated, err := updater(toStorageOfflineSessions(o))
	if err != nil {
		return err
	}

	newOfflineSessions := cli.fromStorageOfflineSessions(updated)
	newOfflineSessions.ObjectMeta = o.ObjectMeta
	return cli.put(resourceOfflineSessions, o.ObjectMeta.Name, newOfflineSessions)
}

func (cli *client) UpdateKeys(updater func(old storage.Keys) (storage.Keys, error)) error {
	firstUpdate := false
	var keys Keys
	if err := cli.get(resourceKeys, keysName, &keys); err != nil {
		if err != storage.ErrNotFound {
			return err
		}
		firstUpdate = true
	}
	var oldKeys storage.Keys
	if !firstUpdate {
		oldKeys = toStorageKeys(keys)
	}

	updated, err := updater(oldKeys)
	if err != nil {
		return err
	}
	newKeys := cli.fromStorageKeys(updated)
	if firstUpdate {
		return cli.post(resourceKeys, newKeys)
	}
	newKeys.ObjectMeta = keys.ObjectMeta
	return cli.put(resourceKeys, keysName, newKeys)
}

func (cli *client) UpdateAuthRequest(id string, updater func(a storage.AuthRequest) (storage.AuthRequest, error)) error {
	var req AuthRequest
	err := cli.get(resourceAuthRequest, id, &req)
	if err != nil {
		return err
	}

	updated, err := updater(toStorageAuthRequest(req))
	if err != nil {
		return err
	}

	newReq := cli.fromStorageAuthRequest(updated)
	newReq.ObjectMeta = req.ObjectMeta
	return cli.put(resourceAuthRequest, id, newReq)
}

func (cli *client) UpdateConnector(id string, updater func(a storage.Connector) (storage.Connector, error)) error {
	var c Connector
	err := cli.get(resourceConnector, id, &c)
	if err != nil {
		return err
	}

	updated, err := updater(toStorageConnector(c))
	if err != nil {
		return err
	}

	newConn := cli.fromStorageConnector(updated)
	newConn.ObjectMeta = c.ObjectMeta
	return cli.put(resourceConnector, id, newConn)
}

func (cli *client) UpdateAuthConfigMap(name string, newConfig storage.AuthConfig, updater func(old,
	new storage.AuthConfig) (storage.AuthConfig, error)) error {

	var authConfig AuthConfigMap
	err := cli.getResource("v1", cli.namespace, resourceConfigMap, name, &authConfig)
	if err != nil {
		return err
	}
	updated, err := updater(toStorageAuthConfig(authConfig), newConfig)
	if err != nil {
		return err
	}
	newAuthConfigMap := cli.fromAuthConfigMap(updated)
	newAuthConfigMap.ObjectMeta = authConfig.ObjectMeta
	return cli.putResource("v1", cli.namespace, resourceConfigMap, name, newAuthConfigMap)
}

func (cli *client) GarbageCollect(now time.Time) (result storage.GCResult, err error) {
	var authRequests AuthRequestList
	if err := cli.list(resourceAuthRequest, &authRequests); err != nil {
		return result, fmt.Errorf("failed to list auth requests: %v", err)
	}

	var delErr error
	for _, authRequest := range authRequests.AuthRequests {
		if now.After(authRequest.Expiry) {
			if err := cli.delete(resourceAuthRequest, authRequest.ObjectMeta.Name); err != nil {
				if err == storage.ErrNotFound {
					continue
				}
				cli.logger.Errorf("failed to delete auth request: %v", err)
				delErr = fmt.Errorf("failed to delete auth request: %v", err)
			}
			result.AuthRequests++
		}
	}
	if delErr != nil {
		return result, delErr
	}

	var authCodes AuthCodeList
	if err := cli.list(resourceAuthCode, &authCodes); err != nil {
		return result, fmt.Errorf("failed to list auth codes: %v", err)
	}

	for _, authCode := range authCodes.AuthCodes {
		if now.After(authCode.Expiry) {
			if err := cli.delete(resourceAuthCode, authCode.ObjectMeta.Name); err != nil {
				if err == storage.ErrNotFound {
					continue
				}
				cli.logger.Errorf("failed to delete auth code %v", err)
				delErr = fmt.Errorf("failed to delete auth code: %v", err)
			}
			result.AuthCodes++
		}
	}
	if delErr != nil {
		return result, delErr
	}

	var refreshTokens RefreshList
	refreshTokenGC := viper.GetInt64("REFRESH_TOKEN_GC")
	if err := cli.list(resourceRefreshToken, &refreshTokens); err != nil {
		return result, fmt.Errorf("failed to list refresh tokens: %v", err)
	}
	for _, refreshToken := range refreshTokens.RefreshTokens {
		expire := refreshToken.CreatedAt.Add(time.Duration(refreshTokenGC) * time.Hour)
		if now.After(expire) {
			if err := cli.delete(resourceRefreshToken, refreshToken.ObjectMeta.Name); err != nil {
				if err == storage.ErrNotFound {
					continue
				}
				cli.logger.Errorf("failed to delete refresh token %v", err)
				delErr = fmt.Errorf("failed to delete refresh token %v", err)
			}
			result.RefreshTokens++
		}
	}

	return result, delErr
}

func (cli *client) CreateClusterRoleBinding(email string) {
	name := "platform_trusted_admin_clusterrolebinding"

	var res k8sapi.ClusterRoleBinding
	err := cli.getResource(RBACApiV1, "", resourceClusterrolebindings, name, &res)
	fmt.Printf("get clusterrolebindings %s: %+v\n\n", name, res)
	if err != nil {
		err = cli.createClusterRoleBinding(name, email)
		fmt.Printf("create clusterrolebindings %s: %+v\n\n", name, err)
	} else {
		err = cli.updateClusterRoleBinding(name, email, res)
		fmt.Printf("update clusterrolebindings %s: %+v\n\n", name, err)
	}

	fmt.Printf("CreateClusterRoleBinding: %+v \n\n", err)
}

func (cli *client) createClusterRoleBinding(name, email string) error {
	// create
	meta := k8sapi.TypeMeta{
		APIVersion: RBACApiV1,
		Kind:       "ClusterRoleBinding",
	}
	r := k8sapi.ClusterRoleBinding{
		TypeMeta: meta,
		ObjectMeta: k8sapi.ObjectMeta{
			Name: name,
		},
		Subjects: []k8sapi.Subject{
			{
				Kind:     "User",
				APIGroup: RBACApiGroup,
				Name:     email,
			},
		},
		RoleRef: k8sapi.RoleRef{
			APIGroup: RBACApiGroup,
			Kind:     "ClusterRole",
			Name:     "alauda_admin",
		},
	}
	return cli.postResource(RBACApiV1, "", resourceClusterrolebindings, r)
}

func (cli *client) updateClusterRoleBinding(name string, email string, res k8sapi.ClusterRoleBinding) error {

	var exists bool
	for _, v := range res.Subjects {
		sub := interface{}(v).(k8sapi.Subject)
		if sub.Name == email {
			//exists
			fmt.Printf("updateClusterRoleBinding: user exist\n")
			exists = true
		} else {
			res.Subjects = append(res.Subjects, k8sapi.Subject{
				Kind:     "User",
				APIGroup: RBACApiGroup,
				Name:     email,
			})
			exists = false
		}
	}
	if exists {
		return nil
	}
	fmt.Printf("updateClusterRoleBinding: res: %+v\n\n", res)

	return cli.putResource(RBACApiV1, "", resourceClusterrolebindings, name, res)
}

func (cli *client) CreateRoleBinding(email string) {

	name := fmt.Sprintf("%s_alauda_namespace_developer_rolebinding", cli.namespace)

	var res k8sapi.RoleBinding
	err := cli.getResource(RBACApiV1, cli.namespace, resourceRolebindings, name, &res)
	fmt.Printf("get rolebindings %s: %+v\n\n", name, res)
	if err != nil {
		err = cli.createRoleBinding(name, email)
		fmt.Printf("create rolebindings %s: %+v\n\n", name, err)
	} else {
		err = cli.updateRoleBinding(name, email, res)
		fmt.Printf("update rolebindings %s: %+v\n\n", name, err)
	}

	fmt.Printf("CreateRoleBinding: %+v \n\n", err)
}

func (cli *client) createRoleBinding(name, email string) error {
	// create
	meta := k8sapi.TypeMeta{
		APIVersion: RBACApiV1,
		Kind:       "RoleBinding",
	}
	r := k8sapi.RoleBinding{
		TypeMeta: meta,
		ObjectMeta: k8sapi.ObjectMeta{
			Name: name,
		},
		Subjects: []k8sapi.Subject{
			{
				Kind:     "User",
				APIGroup: RBACApiGroup,
				Name:     email,
			},
		},
		RoleRef: k8sapi.RoleRef{
			APIGroup: RBACApiGroup,
			Kind:     "Role",
			Name:     "alauda_namespace_developer",
		},
	}
	return cli.postResource(RBACApiV1, cli.namespace, resourceRolebindings, r)
}

func (cli *client) updateRoleBinding(name string, email string, res k8sapi.RoleBinding) error {

	var exists bool
	for _, v := range res.Subjects {
		sub := interface{}(v).(k8sapi.Subject)
		fmt.Printf("\nsub: %+v \n\n", sub)
		if sub.Name == email {
			//exists
			fmt.Printf("updateRoleBinding: user exist\n")
			exists = true
		} else {
			res.Subjects = append(res.Subjects, k8sapi.Subject{
				Kind:     "User",
				APIGroup: RBACApiGroup,
				Name:     email,
			})
			exists = false
		}
	}
	if exists {
		return nil
	}
	fmt.Printf("updateRoleBinding: res: %+v\n\n", res)

	return cli.putResource(RBACApiV1, cli.namespace, resourceRolebindings, name, res)
}
