package storage

var (
	LabelUserName          = "auth.%s/user.name"
	LabelUserUsername      = "auth.%s/user.username"
	LabelUserEmail         = "auth.%s/user.email"
	LabelUserValid         = "auth.%s/user.valid"
	LabelUserConnectorType = "auth.%s/user.connector_type"
	LabelUserConnectorID   = "auth.%s/user.connector_id"

	LabelUserBindingName = "auth.%s/userbinding.name"

	LabelGroupDisplayName = "auth.%s/group.display-name"
	LabelGroupName        = "auth.%s/group.name"

	LabelRoleName               = "auth.%s/role.name"
	LabelRoleLevel              = "auth.%s/role.level"
	LabelRoleVisible            = "auth.%s/role.visible"
	LabelRoleOfficial           = "auth.%s/role.official"
	LabelRoleRelative           = "auth.%s/role.relative"
	LabelRoleBindScope          = "auth.%s/role.bindscope"
	LabelRoleBindNamespaceType  = "auth.%s/role.bindnamespacetype"
	LabelRoleBindNamespaceValue = "auth.%s/role.bindnamespacevalue"
	LabelRoleBindCluster        = "auth.%s/role.bindcluster"

	LabelRoleTemplateOfficial = "auth.%s/roletemplate.official"
	LabelRoleTemplateLevel    = "auth.%s/roletemplate.level"
	LabelRoleTemplateName     = "auth.%s/roletemplate.name" // for labelSelector query

	LabelCreatorEmail = "auth.%s/creator.email"

	LabelCluster     = "%s/cluster"
	LabelClusterName = "%s/cluster.name"
	LabelClusterType = "%s/cluster.type"

	LabelProject       = "%s/project"
	LabelProjectParent = "%s/project.parent"
	LabelProjectLevel  = "%s/project.level"
	LabelNamespace     = "%s/namespace"

	LabelSystemRoleBinding        = "%s/system-rolebinding"
	LabelSystemClusterRoleBinding = "%s/system-clusterrolebinding"

	LabelFunctionResourceRef         = "auth.%s/rt.fr."
	LabelSchemaFunctionResourceRef   = "auth.%s/rt.fr.%s-%s"
	LabelFunctionResourceRefSelector = "auth.%s/role.level,auth.%s/rt.fr.%s,auth.%s/rt.fr.%s='true'"
)
