package storage

import (
	"fmt"
)

func PopulateWithLabelBaseDomain(labelBaseDomain string) {
	populateLabels(labelBaseDomain)
	populateAnnotations(labelBaseDomain)
}

func populateLabels(labelBaseDomain string) {
	LabelUserName = fmt.Sprintf(LabelUserName, labelBaseDomain)
	LabelUserUsername = fmt.Sprintf(LabelUserUsername, labelBaseDomain)
	LabelUserEmail = fmt.Sprintf(LabelUserEmail, labelBaseDomain)
	LabelUserValid = fmt.Sprintf(LabelUserValid, labelBaseDomain)
	LabelUserConnectorType = fmt.Sprintf(LabelUserConnectorType, labelBaseDomain)
	LabelUserConnectorID = fmt.Sprintf(LabelUserConnectorID, labelBaseDomain)

	LabelUserBindingName = fmt.Sprintf(LabelUserBindingName, labelBaseDomain)

	LabelGroupDisplayName = fmt.Sprintf(LabelGroupDisplayName, labelBaseDomain)

	LabelRoleName = fmt.Sprintf(LabelRoleName, labelBaseDomain)
	LabelRoleLevel = fmt.Sprintf(LabelRoleLevel, labelBaseDomain)
	LabelRoleVisible = fmt.Sprintf(LabelRoleVisible, labelBaseDomain)
	LabelRoleRelative = fmt.Sprintf(LabelRoleRelative, labelBaseDomain)
	LabelRoleOfficial = fmt.Sprintf(LabelRoleOfficial, labelBaseDomain)
	LabelRoleBindScope = fmt.Sprintf(LabelRoleBindScope, labelBaseDomain)
	LabelRoleBindNamespaceType = fmt.Sprintf(LabelRoleBindNamespaceType, labelBaseDomain)
	LabelRoleBindNamespaceValue = fmt.Sprintf(LabelRoleBindNamespaceValue, labelBaseDomain)
	LabelRoleBindCluster = fmt.Sprintf(LabelRoleBindCluster, labelBaseDomain)

	LabelRoleTemplateOfficial = fmt.Sprintf(LabelRoleTemplateOfficial, labelBaseDomain)
	LabelRoleTemplateLevel = fmt.Sprintf(LabelRoleTemplateLevel, labelBaseDomain)
	LabelRoleTemplateName = fmt.Sprintf(LabelRoleTemplateName, labelBaseDomain)

	LabelCreatorEmail = fmt.Sprintf(LabelCreatorEmail, labelBaseDomain)

	LabelProject = fmt.Sprintf(LabelProject, labelBaseDomain)
	LabelProjectParent = fmt.Sprintf(LabelProjectParent, labelBaseDomain)
	LabelProjectLevel = fmt.Sprintf(LabelProjectLevel, labelBaseDomain)

	LabelNamespace = fmt.Sprintf(LabelNamespace, labelBaseDomain)
	LabelCluster = fmt.Sprintf(LabelCluster, labelBaseDomain)
	LabelClusterName = fmt.Sprintf(LabelClusterName, labelBaseDomain)
	LabelClusterType = fmt.Sprintf(LabelClusterType, labelBaseDomain)

	LabelSystemRoleBinding = fmt.Sprintf(LabelSystemRoleBinding, labelBaseDomain)
	LabelSystemClusterRoleBinding = fmt.Sprintf(LabelSystemClusterRoleBinding, labelBaseDomain)

	LabelFunctionResourceRef = fmt.Sprintf(LabelFunctionResourceRef, labelBaseDomain)
	LabelSchemaFunctionResourceRef = fmt.Sprintf(LabelSchemaFunctionResourceRef, labelBaseDomain, "%s", "%s")
	LabelFunctionResourceRefSelector = fmt.Sprintf(LabelFunctionResourceRefSelector, labelBaseDomain, labelBaseDomain, "%s", labelBaseDomain)
}

func populateAnnotations(labelBaseDomain string) {
	AnnotationForceDelete = fmt.Sprintf(AnnotationForceDelete, labelBaseDomain)
	AnnotationDisplayName = fmt.Sprintf(AnnotationDisplayName, labelBaseDomain)
	AnnotationDisplayNameEn = fmt.Sprintf(AnnotationDisplayNameEn, labelBaseDomain)
	AnnotationProduct = fmt.Sprintf(AnnotationProduct, labelBaseDomain)
	AnnotationProductVersion = fmt.Sprintf(AnnotationProductVersion, labelBaseDomain)
	AnnotationRoleVersion = fmt.Sprintf(AnnotationRoleVersion, labelBaseDomain)
	AnnotationProject = fmt.Sprintf(AnnotationProject, labelBaseDomain)
	AnnotationProjectLevel = fmt.Sprintf(AnnotationProjectLevel, labelBaseDomain)
	AnnotationProjectParent = fmt.Sprintf(AnnotationProjectParent, labelBaseDomain)
	AnnotationCluster = fmt.Sprintf(AnnotationCluster, labelBaseDomain)
	AnnotationClusterName = fmt.Sprintf(AnnotationClusterName, labelBaseDomain)
	AnnotationClusterType = fmt.Sprintf(AnnotationClusterType, labelBaseDomain)
	AnnotationClusterAttr = fmt.Sprintf(AnnotationClusterAttr, labelBaseDomain)
	AnnotationPipelineLastNumber = fmt.Sprintf(AnnotationPipelineLastNumber, labelBaseDomain)
	AnnotationPipelineNumber = fmt.Sprintf(AnnotationPipelineNumber, labelBaseDomain)
	AnnotationPipelineConfig = fmt.Sprintf(AnnotationPipelineConfig, labelBaseDomain)
	AnnotationJenkinsBuildURI = fmt.Sprintf(AnnotationJenkinsBuildURI, labelBaseDomain)
	AnnotationSecretType = fmt.Sprintf(AnnotationSecretType, labelBaseDomain)
	AnnotationCreateAppUrl = fmt.Sprintf(AnnotationCreateAppUrl, labelBaseDomain)
	AnnotationUpdatedAt = fmt.Sprintf(AnnotationUpdatedAt, labelBaseDomain)
	AnnotationDescription = fmt.Sprintf(AnnotationDescription, labelBaseDomain)
	AnnotationCreator = fmt.Sprintf(AnnotationCreator, labelBaseDomain)
	AnnotationCurrentCluster = fmt.Sprintf(AnnotationCurrentCluster, labelBaseDomain)

	AnnotationUserEmail = fmt.Sprintf(AnnotationUserEmail, labelBaseDomain)

}
