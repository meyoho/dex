FROM golang:1.13-alpine

ARG app_version=2.2.0

RUN apk add --no-cache --update alpine-sdk upx

COPY . $GOPATH/src/github.com/coreos/dex
WORKDIR $GOPATH/src/github.com/coreos/dex
RUN go build  -o ./bin/dex -v -ldflags "-w -X github.com/coreos/dex/version.Version=${app_version}" -a -installsuffix cgo ./cmd/dex

FROM alpine:3.11
# Dex connectors, such as GitHub and Google logins require root certificates.
# Proper installations should manage those certificates, but it's a bad user
# experience when this doesn't work out of the box.
#
# OpenSSL is required so wget can query HTTPS endpoints for health checking.
RUN apk add --update ca-certificates openssl

COPY --from=0 //go/src/github.com/coreos/dex/bin/dex /usr/local/bin/dex

# Import frontend assets and set the correct CWD directory so the assets
# are in the default path.
COPY web /web
WORKDIR /

ENTRYPOINT ["dex"]

CMD ["version"]
