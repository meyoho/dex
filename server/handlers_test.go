package server

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
	"github.com/gorilla/mux"
)

func TestHandleHealth(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	httpServer, server := newTestServer(ctx, t, nil)
	defer httpServer.Close()

	rr := httptest.NewRecorder()
	server.handleHealth(rr, httptest.NewRequest("GET", "/healthz", nil))
	if rr.Code != http.StatusOK {
		t.Errorf("expected 200 got %d", rr.Code)
	}
}

func TestHandleConnectorLogin(t *testing.T)  {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	httpServer, server := newTestServer2(ctx, t, nil)
	defer httpServer.Close()

	w := httptest.NewRecorder()
	r, _ := http.NewRequest("GET", "/auth/mocksimpletoken?token=tstack_token&client_id=alauda-auth&redirect_uri=http://localhost:4200", nil)

	r = mux.SetURLVars(r, map[string]string{"connector": "mocksimpletoken"})

	server.handleConnectorLogin(w, r)
}

func TestHandleAuthorization(t *testing.T)  {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	httpServer, server := newTestServer2(ctx, t, nil)
	defer httpServer.Close()

	w := httptest.NewRecorder()
	r, _ := http.NewRequest("GET", "/auth?access_type=offline&client_id=alauda-auth&nonce=rng&redirect_uri=http%3a%2f%2flocalhost%3a4200%3fref%3dmocksimpletoken&response_type=code&scope=openid+profile+email+groups", nil)

	server.handleAuthorization(w, r)
}

