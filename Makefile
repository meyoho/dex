.PHONY: init build-base build unitest test
PROJ=dex
ORG_PATH=github.com/coreos
REPO_PATH=$(ORG_PATH)/$(PROJ)
PWD=$(shell pwd)
TAG=dev-$(shell cat .version)-$(shell git config --get user.email | sed -e "s/@/-/")
IMAGE=index.alauda.cn/alaudak8s/dex
#IMAGE=index.alauda.cn/alaudaorg/dex
image=$(IMAGE)
tag=$(TAG)
BASE_TOKEN=$(shell echo $(TAG))-int-dev
INT_TOKEN=$(shell echo $(BASE_TOKEN) | sed 's|\.|-|g')
# GOPATH=$(PWD)/.bin
LD_FLAGS="-w -X $(REPO_PATH)/version.Version=$(VERSION)"
VERSION ?= $(shell ./scripts/git-version)
$(shell mkdir -p bin )
PACKAGES=$(shell go list ./...)
#$( shell mkdir -p .bin/src/github.com/coreos/dex )
#$( shell mkdir -p .bin/bin )

prepare:
	docker build -t dex-build -f Docker.build .

prepare-docker:
	cp Dockerfile.ci ./bin/Dockerfile
	cp -r web ./bin/web

run-build: prepare-docker
	GOOS=linux go build -o bin/dex -v -ldflags $(LD_FLAGS) -a -installsuffix cgo $(REPO_PATH)/cmd/dex

run-cover:
	echo "mode: count" > bin/coverage-all.out
	@$(foreach pkg,$(PACKAGES),\
		go test -v -coverprofile=coverage.out -covermode=count $(pkg);\
		if [ -f coverage.out ]; then\
			tail -n +2 coverage.out >> bin/coverage-all.out;\
		fi;)

run-local:
	go test -cover -v $(PACKAGES)

run-test:
	go test -cover -v $(PACKAGES) -json > bin/test.json

build: prepare
	docker run --rm -v $(PWD):/go/src/$(REPO_PATH) -w /go/src/$(REPO_PATH) dex-build make run-build

build-image: build prepare-docker
	docker build -t ${IMAGE}:${TAG} -f ./bin/Dockerfile ./bin
	rm -rf ./bin

test: prepare
	docker run --rm -v $(PWD):/go/src/$(REPO_PATH) -w /go/src/$(REPO_PATH) dex-build make run-test

cover: prepare
	docker run --rm -v $(PWD):/go/src/$(REPO_PATH) -w /go/src/$(REPO_PATH) dex-build make run-cover

push-image: build-image
	docker push ${IMAGE}:${TAG}

run: push-image
	$(MAKE) image=${IMAGE} tag=${TAG} update

update:
	kubectl set image deploy/dex -n alauda-system dex=$(image):$(tag)
	make restart

clean:
	rm -rf ./bin

restart:
	kubectl scale deploy -n alauda-system dex --replicas=0
	sleep 1
	kubectl scale deploy -n alauda-system dex --replicas=1