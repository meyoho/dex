package themex

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"log"

	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"

	"bitbucket.org/mathildetech/themex/api"
	"bitbucket.org/mathildetech/themex/products/api/types/v1alpha1"
	"fmt"
	"k8s.io/client-go/tools/clientcmd"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	cli              *Client
	once             sync.Once
	PortalNamespace  string
	PortalConfigName string
)

func (c *Config) getKubeConfig() (*rest.Config, error) {
	// get home kube config
	if c.KubeConfigFile == "" {
		c.KubeConfigFile = os.Getenv("HOME") + "/.kube/config"
	}
	// check file exist
	if PathExist(c.KubeConfigFile) {
		// Check if the current cluster is minikube
		rules := clientcmd.ClientConfigLoadingRules{ExplicitPath: c.KubeConfigFile}
		cfg, err := rules.Load()
		if err == nil && cfg.CurrentContext == "minikube" {
			if c.InMinikube == false {
				c.InMinikube = true
			}
		}
		log.Println("custome theme kubeConfigFile:", c.KubeConfigFile)
	} else {
		c.KubeConfigFile = ""
		log.Println("custome theme kubeConfigFile does not exists!")
	}

	PortalNamespace = viper.GetString("PORTAL_NAMESPACE")
	if PortalNamespace == "" {
		PortalNamespace = PortalDefaultNamespace
	}
	PortalConfigName = viper.GetString("PORTAL_CONFIG_NAME")
	if PortalConfigName == "" {
		PortalConfigName = PortalDefaultConfigName
	}
	log.Println("PortalNamespace: ", PortalNamespace, " PortalConfigName: ", PortalConfigName)

	return clientcmd.BuildConfigFromFlags("", c.KubeConfigFile)
}

type Client struct {
	manager *api.Manager
}

// Init custome theme client for config
func StartWithConfigure(config *Config) error {
	log.Printf("Init custom theme client, version is %s\n", Version())
	var err error
	cfg, err := config.getKubeConfig()
	if err != nil {
		return err
	}

	once.Do(func() {
		// Init AlaudaProduct crd
		v1alpha1.AddToScheme(scheme.Scheme)

		// init k8s api manager
		manager := api.NewForConfig(cfg, config.InMinikube)

		cli = &Client{
			manager: manager,
		}
	})
	return err
}

func Version() string {
	return ClientVersion
}

/* Help methods for action with most common PortalConfigName in PortalNamespace */
func getConfigMap(namespace, name string) (*corev1.ConfigMap, error) {
	if cli == nil {
		return nil, ErrClientNil
	}
	k8sClient, err := cli.manager.Client()
	if err != nil {
		return nil, err
	}
	cm, err := cli.manager.GetConfigMap(k8sClient, namespace, name, metav1.GetOptions{})

	if err != nil {
		return nil, err
	}

	return cm, err
}

func deleteConfigMap(namespace, name string) error {
	if cli == nil {
		return ErrClientNil
	}
	k8sClient, err := cli.manager.Client()
	if err != nil {
		return err
	}

	return cli.manager.DeleteConfigMap(k8sClient, namespace, name, &metav1.DeleteOptions{})
}

func UpdateConfigMapByKey(namespace, name, key, value string) (*corev1.ConfigMap, error) {
	if cli == nil {
		return nil, ErrClientNil
	}
	k8sClient, err := cli.manager.Client()
	if err != nil {
		return nil, err
	}
	cm, err := cli.manager.GetConfigMap(k8sClient, namespace, name, metav1.GetOptions{})

	if err != nil {
		return nil, err
	}

	cm.Data[key] = value

	return cli.manager.UpdateConfigMap(k8sClient, namespace, cm)
}

func getPortalConfigMap() (*corev1.ConfigMap, error) {
	return getConfigMap(PortalNamespace, PortalConfigName)
}

func updatePortalConfigMap(key, value string) error {
	_, err := UpdateConfigMapByKey(PortalNamespace, PortalConfigName, key, value)

	return err
}

/* Theme operations */
// RetrieveTheme get theme data from configmap
func RetrieveTheme() (*Theme, error) {
	cm, err := getConfigMap(PortalNamespace, PortalConfigName)

	if err != nil {
		return nil, err
	}

	theme := Theme{}

	strConfig := cm.Data[ThemeKey]

	if strConfig == "" {
		return nil, ErrThemeNil
	}

	err = yaml.Unmarshal([]byte(strConfig), &theme)
	if err != nil {
		return nil, err
	}

	return &theme, nil
}

// UpdateTheme update the data stored in configmap
func UpdateTheme(theme *Theme) (*Theme, error) {
	byteConfig, err := yaml.Marshal(theme)
	if err != nil {
		return nil, ErrThemeInvalid
	}

	err = updatePortalConfigMap(ThemeKey, string(byteConfig))

	return theme, err
}

/* Localization operations */
// ListLocalizations will list the all localization information stored in configmap
func ListLocalizations() ([]*Localization, error) {
	cm, err := getConfigMap(PortalNamespace, PortalConfigName)
	if err != nil {
		return nil, err
	}

	locals := make([]*Localization, 0)

	strConfig := cm.Data[LocalizationKey]
	if strConfig != "" {
		err = yaml.Unmarshal([]byte(strConfig), &locals)
		if err != nil {
			return nil, err
		}
	}

	return locals, nil
}

// UpdateLocalization will update the localization information stored in configmap
func UpdateLocalization(local *Localization) (*Localization, error) {
	locals, err := ListLocalizations()
	if err != nil {
		return nil, err
	}

	var valid bool

	for _, l := range locals {
		if l.Name == local.Name {
			l.ZH = local.ZH
			l.EN = local.EN
			valid = true
			break
		}
	}

	// if no localization with specified name exists
	if !valid {
		return nil, ErrLocalizationNotExist
	}

	byteConfig, err := yaml.Marshal(locals)
	if err != nil {
		return nil, err
	}

	err = updatePortalConfigMap(LocalizationKey, string(byteConfig))

	return local, err
}

/* Logo operations */
// ListLogos return logos data stored in configmap
func ListLogos() ([]*Logo, error) {
	cm, err := getConfigMap(PortalNamespace, PortalConfigName)
	if err != nil {
		return nil, err
	}

	strConfig := cm.Data[LogoKey]

	logos := make([]*Logo, 0)

	if strConfig != "" {
		err = yaml.Unmarshal([]byte(strConfig), &logos)
		if err != nil {
			return nil, err
		}
	}

	return logos, nil
}

// RetrieveLogo return logo data stored in configmap
func RetrieveLogo(name string) (*Logo, error) {
	log.Printf("Getting logo information for: %s\n", name)
	logos, err := ListLogos()

	name = FormatName(name)

	if err != nil {
		return nil, ErrLogoNotExist
	}

	for _, logo := range logos {
		if logo.Name == name {
			return logo, nil
		}
	}

	return nil, ErrLogoNotExist
}

// UpdateLogo update the logo data stored in configmap
func UpdateLogo(logo *Logo) (*Logo, error) {
	log.Printf("Getting logo information for: %s\n", logo)

	logos, err := ListLogos()

	if err != nil {
		return nil, err
	}

	var valid bool

	for _, l := range logos {
		if l.Name == logo.Name {
			l.Url = logo.Url

			l.Url = AddTimestamp(l.Url)

			logo.Url = l.Url
			valid = true
			break
		}
	}

	// if no localization with specified name exists
	if !valid {
		return nil, ErrLogoNotExist
	}

	byteConfig, err := yaml.Marshal(logos)
	if err != nil {
		return nil, err
	}

	err = updatePortalConfigMap(LogoKey, string(byteConfig))

	return logo, nil
}

/* Banner operations */
// ListBanners will return the banners info stored in configmap
func ListBanners() ([]*Banner, error) {
	cm, err := getConfigMap(PortalNamespace, PortalConfigName)
	if err != nil {
		return nil, err
	}

	strConfig := cm.Data[BannerKey]

	banners := make([]*Banner, 0)

	if strConfig != "" {
		err = yaml.Unmarshal([]byte(strConfig), &banners)
		if err != nil {
			return nil, err
		}
	}

	return banners, nil
}

// CreateBanner
func CreateBanner(banner *Banner) (*Banner, error) {
	banners, err := ListBanners()
	if err != nil {
		return nil, err
	}

	var exist bool

	for _, b := range banners {
		if b.Name == banner.Name {
			b.Image = banner.Image
			b.Url = banner.Url
			b.Description = banner.Description
			exist = true
			break
		}
	}

	// if no banner with specified name exists
	if exist {
		return nil, ErrBannerAlreadyExist
	}

	banners = append(banners, banner)

	byteConfig, err := yaml.Marshal(banners)
	if err != nil {
		return nil, err
	}

	err = updatePortalConfigMap(BannerKey, string(byteConfig))

	return banner, err
}

// RetrieveBanner
func RetrieveBanner(name string) (*Banner, error) {
	banners, err := ListBanners()
	if err != nil {
		return nil, err
	}

	for _, b := range banners {
		if b.Name == name {
			return b, nil
		}
	}

	return nil, ErrBannerNotExist
}

// UpdateBanner
func UpdateBanner(banner *Banner) (*Banner, error) {
	banners, err := ListBanners()
	if err != nil {
		return nil, err
	}

	var valid bool

	for _, b := range banners {
		if b.Name == banner.Name {
			b.Image = banner.Image
			b.Url = banner.Url
			b.Url = AddTimestamp(b.Url)
			b.Description = banner.Description
			valid = true
			break
		}
	}

	// if no banner with specified name exists
	if !valid {
		return nil, ErrBannerNotExist
	}

	byteConfig, err := yaml.Marshal(banners)
	if err != nil {
		return nil, err
	}

	err = updatePortalConfigMap(BannerKey, string(byteConfig))

	return banner, err
}

// DeleteBanner
func DeleteBanner(name string) error {
	banners, err := ListBanners()
	if err != nil {
		return err
	}

	var found bool

	result := make([]*Banner, 0)

	for _, b := range banners {
		if b.Name == name {
			found = true
		} else {
			result = append(result, b)
		}
	}

	// if no banner with specified name exists
	if !found {
		return ErrBannerNotExist
	}

	byteConfig, err := yaml.Marshal(result)
	if err != nil {
		return err
	}

	return updatePortalConfigMap(BannerKey, string(byteConfig))
}

/* Product operations */
// checkProductLables ensure that Labels attribute exists
func checkProductLabels(k8sProduct *v1alpha1.AlaudaProduct) *v1alpha1.AlaudaProduct {
	if len(k8sProduct.Labels) == 0 {
		k8sProduct.Labels = make(map[string]string)
	}

	return k8sProduct
}

// setProduct set information from AlaudaProduct to products/api/types/v1alpha1.AlaudaProduct
func setProduct(v *AlaudaProduct, k8sProduct *v1alpha1.AlaudaProduct) *v1alpha1.AlaudaProduct {
	// alauda.io/product.[deploy|sort|provider] label?
	k8sProduct.Spec.Displayname.CN = v.DisplayName.CN
	k8sProduct.Spec.Displayname.EN = v.DisplayName.EN
	k8sProduct.Spec.Description.CN = v.Description.CN
	k8sProduct.Spec.Description.EN = v.Description.EN

	k8sProduct.Spec.Icon = AddTimestamp(v.Icon)
	k8sProduct.Spec.CrdVersion = v.Version
	k8sProduct.Spec.Target = v.Target
	k8sProduct.Spec.Homepage = v.Homepage

	k8sProduct.Name = v.Name

	if len(k8sProduct.Labels) == 0 {
		k8sProduct.Labels = make(map[string]string)
	}

	// label changing
	if v.Sort != k8sProduct.Labels["alauda.io/product.sort"] {
		k8sProduct.Labels["alauda.io/product.sort"] = v.Sort
	}
	if v.Provider != k8sProduct.Labels["alauda.io/product.provider"] {
		k8sProduct.Labels["alauda.io/product.provider"] = v.Provider
	}

	return k8sProduct
}

// getProduct convert products/api/types/v1alpha1.AlaudaProduct to AlaudaProduct
func getProduct(v *v1alpha1.AlaudaProduct) *AlaudaProduct {
	isDeployed, _ := strconv.ParseBool(v.Labels["alauda.io/product.deploy"])
	return &AlaudaProduct{
		Name: v.Name,
		Icon: v.Spec.Icon,
		Description: AlaudaProductLang{
			CN: v.Spec.Description.CN,
			EN: v.Spec.Description.EN,
		},
		Version: v.Spec.CrdVersion,
		DisplayName: AlaudaProductLang{
			CN: v.Spec.Displayname.CN,
			EN: v.Spec.Displayname.EN,
		},
		Target:     v.Spec.Target,
		IsDeployed: isDeployed,
		Sort:       v.Labels["alauda.io/product.sort"],
		Provider:   v.Labels["alauda.io/product.provider"],
		Homepage:   v.Spec.Homepage,
	}
}

// ListProducts
func ListProducts() ([]*AlaudaProduct, error) {
	if cli == nil {
		return nil, ErrClientNil
	}
	crdClient, err := cli.manager.ProductClient()
	if err != nil {
		return nil, err
	}

	// alaudaproducts is not namespaced resource
	list, err := crdClient.Projects(PortalNamespace).List(metav1.ListOptions{})
	if err != nil {
		return nil, err
	}

	products := make([]*AlaudaProduct, 0)

	sort.Sort(list.Items)
	for _, v := range list.Items {
		product := getProduct(&v)

		products = append(products, product)
	}

	return products, nil
}

// RetrieveProduct
func RetrieveProduct(name string) (*AlaudaProduct, error) {
	products, err := ListProducts()
	if err != nil {
		return nil, err
	}

	for _, p := range products {
		if p.Name == name {
			return p, nil
		}
	}

	return nil, ErrProductNotExist
}

// UpdateProduct
func UpdateProduct(product *AlaudaProduct) (*AlaudaProduct, error) {
	if cli == nil {
		return nil, ErrClientNil
	}
	crdClient, err := cli.manager.ProductClient()
	if err != nil {
		return nil, err
	}

	k8sProduct, err := crdClient.Projects(PortalNamespace).Get(product.Name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	k8sProduct = setProduct(product, k8sProduct)

	// alaudaproducts is not namespaced resource
	_, err = crdClient.Projects(PortalNamespace).Update(k8sProduct)

	return product, err
}

/* Image operations */

// RetrieveImage
func RetrieveImage(imgType, name string) (*Image, error) {
	name = FormatName(name)
	// portal no use of icon, included in product
	if imgType == "product" {
		imgType = "icon"
	}
	configName := fmt.Sprintf("%s-%s", name, imgType)

	cm, err := getConfigMap(PortalNamespace, configName)

	if err != nil {
		return nil, err
	}

	strConfig := cm.Data[ImageKey]
	if strConfig == "" {
		return nil, ErrImageNotExist
	}

	contentType := cm.Data[ImageContentTypeKey]
	if contentType == "" {
		return nil, ErrImageNotExist
	}

	return NewImage(imgType, name, strConfig, contentType), nil
}

// UpdateImage
func UpdateImage(img *Image) error {

	imgName := FormatName(img.Name)
	_, err := RetrieveImage(img.Type, imgName)
	if err != nil {
		return err
	}
	configName := fmt.Sprintf("%s-%s", imgName, img.Type)

	_, err = UpdateConfigMapByKey(PortalNamespace, configName, ImageKey, img.Data)
	if err != nil {
		return err
	}

	_, err = UpdateConfigMapByKey(PortalNamespace, configName, ImageContentTypeKey, img.ContentType)
	if err != nil {
		return err
	}

	switch img.Type {
	case TypeLogo:
		logo, errUpdate := RetrieveLogo(imgName)
		if errUpdate == nil {
			UpdateLogo(logo)
		}
	case TypeBanner:
		banner, errUpdate := RetrieveBanner(imgName)
		if errUpdate == nil {
			UpdateBanner(banner)
		}
	case TypeIcon, TypeProduct:
		product, errUpdate := RetrieveProduct(img.Name)
		if errUpdate == nil {
			UpdateProduct(product)
		}
	}
	return err
}

// DeleteImage
func DeleteImage(imgType, name string) error {
	// portal no use of icon, included in product
	if imgType == "product" {
		imgType = "icon"
	}
	configName := fmt.Sprintf("%s-%s", imgType, name)

	return deleteConfigMap(PortalNamespace, configName)
}

/* Shortcut for retrieve configuration for alaudaproduct */
func RetrievePlatformConfigurations(name string) (*Platform, error) {
	// get theme
	theme, err := RetrieveTheme()
	if err != nil {
		return nil, err
	}

	// get favicon
	favicon, err := RetrieveLogo("favicon")
	if err != nil {
		return nil, err
	}

	// get logo
	logoName := FormatName(name)
	logo, err := RetrieveLogo(logoName)
	if err != nil {
		return nil, err
	}

	// get title
	var title string
	if name == "portal" {
		title = PortalTitle
	} else if name == "dex" {
		title = DexTitle
	} else {
		// get alaudaproduct.displayName.cn
		product, err := RetrieveProduct(name)
		if err != nil {
			return nil, err
		}

		title = product.DisplayName.CN
	}

	return &Platform{
		Name:       name,
		Logo:       logo.Url,
		ThemeName:  theme.Name,
		ThemeColor: theme.Color,
		Title:      title,
		Favicon:    favicon.Url,
	}, nil

}

// FormatName will convert the `name` to the name stored in cm
// acp-doc-* will return acp-doc
// acp-* will return acp
// alauda-kubernetes will return ake
// otherwise return `name`
func FormatName(name string) (result string) {
	if strings.HasPrefix(name, "acp-doc-") {
		result = "acp-doc"
	} else if strings.HasPrefix(name, "acp-") {
		result = "acp"
	} else if name == AlaudaKubernetes {
		result = "aks"
	} else {
		result = name
	}

	return
}

// AddTimestamp add a timestamp query parameter
func AddTimestamp(url string) (result string) {
	timestamp := fmt.Sprintf("%d", time.Now().Unix())
	if strings.Contains(url, "?") {
		// replace existing timestamp
		urls := strings.Split(url, "?")
		urls = append(urls[:len(urls)-1], timestamp)
		result = strings.Join(urls, "?")
	} else {
		// add timestamp
		result = fmt.Sprintf("%s?%s", url, timestamp)
	}

	return
}

func PathExist(_path string) bool {
	_, err := os.Stat(_path)
	if err != nil && os.IsNotExist(err) {
		return false
	}
	return true
}
