package handler

import (
	"bitbucket.org/mathildetech/themex"
	"bitbucket.org/mathildetech/themex/configuration"
	kdErrors "bitbucket.org/mathildetech/themex/errors"
	"encoding/base64"
	"github.com/emicklei/go-restful"
	"net/http"
	"strings"
)

// APIHandler manages all endpoints related to configuration.
type APIHandler struct {
	Module string
}

func NewAPIHandler(module string) *APIHandler {
	return &APIHandler{
		Module: module,
	}
}

// Install creates new endpoints for dashboard auth, such as login. It allows user to log in to dashboard using
// one of the supported methods. See AuthManager and Authenticator for more information.
func (apiHandler *APIHandler) Install(ws *restful.WebService) {
	ws.Route(
		ws.GET("/configuration").
			To(apiHandler.handleConfiguration).
			Writes(configuration.Configuration{}))

	ws.Route(
		ws.PUT("/configuration").
			To(apiHandler.handleUpdateConfiguration).
			Writes(configuration.Configuration{}))
}

// Configuration diagnose
func (apiHandler *APIHandler) handleConfiguration(request *restful.Request, response *restful.Response) {
	configType := request.QueryParameter("type")
	name := request.QueryParameter("name")

	var result configuration.Configuration
	result.Name = name
	result.Type = configType

	var (
		data interface{}
		err  error
	)

	if name != "" {
		data, err = configuration.GetConfiguration(configType, name)
	} else {
		data, err = configuration.ListConfiguration(configType)
	}
	result.Data = data
	result.Errors = []error{}
	if err != nil {
		result.Errors = []error{kdErrors.FormatError(err)}
	}

	response.WriteHeaderAndEntity(http.StatusOK, result)
}

// handleUpdateConfiguration
func (apiHandler *APIHandler) handleUpdateConfiguration(request *restful.Request, response *restful.Response) {
	configType := request.QueryParameter("type")
	name := request.QueryParameter("name")

	config := new(configuration.Configuration)
	config.Errors = []error{}

	contentType := request.Request.Header.Get(restful.HEADER_ContentType)
	if contentType == restful.MIME_JSON {
		if err := request.ReadEntity(config); err != nil {
			config.Errors = append(config.Errors, kdErrors.FormatError(err))
		}
	} else {
		// handling file upload
		err := request.Request.ParseMultipartForm(0)
		if err != nil {
			config.Errors = append(config.Errors, kdErrors.FormatError(err))
		}

		// the FormFile function takes in the POST input id file
		file, header, err := request.Request.FormFile("data")
		if err != nil {
			config.Errors = append(config.Errors, kdErrors.FormatError(err))
		}

		defer file.Close()

		content := make([]byte, header.Size)
		_, err = file.Read(content)
		if err != nil {
			config.Errors = append(config.Errors, kdErrors.FormatError(err))
		}
		fileContentType := detectContentType(content)
		data := make(map[string]string)
		data["content_type"] = fileContentType
		names := strings.Split(name, "-")
		if len(names) < 2 {
			data["name"] = name
		} else {
			data["name"] = strings.Join(names[:len(names)-1], "-")
			data["type"] = names[len(names)-1]
		}

		if fileContentType == themex.ImageContentTypeSvg {
			data["data"] = string(content)
		} else {
			data["data"] = base64.StdEncoding.EncodeToString(content)
		}
		config.Data = data

	}

	err := configuration.UpdateConfiguration(configType, name, config)

	if err != nil {
		config.Errors = append(config.Errors, kdErrors.FormatError(err))
	}

	response.WriteHeaderAndEntity(http.StatusOK, config)
}

func detectContentType(data []byte) string {
	contentType := http.DetectContentType(data)

	if strings.HasPrefix(contentType, "text/plain") {
		return themex.ImageContentTypeSvg
	}

	return contentType
}
