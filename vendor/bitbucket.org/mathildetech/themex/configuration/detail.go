// Copyright 2017 The Kubernetes Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package configuration

import (
	"bitbucket.org/mathildetech/themex"
	"encoding/json"
)

// GetConfiguration returns specified configuration with name.
func GetConfiguration(configType, name string) (result interface{}, err error) {
	switch configType {
	case themex.TypeTheme:
		result, err = themex.RetrieveTheme()
	case themex.TypePlatform:
		result, err = themex.RetrievePlatformConfigurations(name)
	case themex.TypeLogo:
		result, err = themex.RetrieveLogo(name)
	case themex.TypeLocalization:
		err = themex.ErrNotSupportedAction
		// config, err := themex.RetrieveLocalization(name)
	case themex.TypeProduct:
		result, err = themex.RetrieveProduct(name)
	case themex.TypeBanner:
		err = themex.ErrNotSupportedAction
		// config, err := themex.RetrieveBanner(name)
	default:
		err = themex.ErrNotSupportedType
	}

	return
}

// GetConfiguration returns specified configuration with name.
func UpdateConfiguration(configType, name string, config *Configuration) (err error) {
	data, err := json.Marshal(config.Data)
	if err != nil {
		return err
	}

	switch configType {
	case themex.TypeTheme:
		var theme themex.Theme
		err = json.Unmarshal(data, &theme)
		if err != nil {
			return err
		}
		_, err = themex.UpdateTheme(&theme)
	case themex.TypeLocalization:
		var local themex.Localization
		err = json.Unmarshal(data, &local)
		if err != nil {
			return err
		}
		_, err = themex.UpdateLocalization(&local)
	case themex.TypeProduct:
		var product themex.AlaudaProduct
		err = json.Unmarshal(data, &product)
		if err != nil {
			return err
		}
		_, err = themex.UpdateProduct(&product)
	case themex.TypeBanner:
		var banner themex.Banner
		err = json.Unmarshal(data, &banner)
		if err != nil {
			return err
		}
		_, err = themex.UpdateBanner(&banner)
	case themex.TypeLogo:
		var logo themex.Logo
		err = json.Unmarshal(data, &logo)
		if err != nil {
			return err
		}
		_, err = themex.UpdateLogo(&logo)
	case themex.TypeImage:
		var image themex.Image
		err = json.Unmarshal(data, &image)
		if err != nil {
			return err
		}
		err = themex.UpdateImage(&image)
	default:
		// handle TypePlatform/TypeLogo
		err = themex.ErrNotSupportedType
	}

	return
}
