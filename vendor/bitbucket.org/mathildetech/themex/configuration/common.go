package configuration

type Configuration struct {
	Name string      `json:"name"`
	Type string      `json:"type"`
	Data interface{} `json:"data"`

	Errors []error `json:"errors"`
}
